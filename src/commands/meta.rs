use serenity::framework::standard::{macros::command, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;
use chrono::prelude::*;

const VERSION: &'static str = env!("CARGO_PKG_VERSION");

#[command]
async fn ping(ctx: &Context, msg: &Message) -> CommandResult {
    msg.channel_id.say(&ctx.http, "Pang!").await?;

    Ok(())
}

#[command]
async fn version(ctx: &Context, msg: &Message) -> CommandResult {
    msg.channel_id.say(&ctx.http, VERSION).await?;

    Ok(())
}

#[command]
async fn time(ctx: &Context, msg: &Message) -> CommandResult {
    let utc: DateTime<Utc> = Utc::now();
    //let local: DateTime<Local> = Local::now();

    let output = utc.format("%T");
    msg.channel_id.say(&ctx.http, output).await?;

    Ok(())
}

#[command]
async fn date(ctx: &Context, msg: &Message) -> CommandResult {
    let utc: DateTime<Utc> = Utc::now();

    let output = utc.format("%Y-%m-%d");
    msg.channel_id.say(&ctx.http, output).await?;

    Ok(())
}
