use serenity::framework::standard::{macros::command, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;
//use std::thread::sleep;
use std::time::Duration;
use rand::SeedableRng;
use rand::rngs::SmallRng;
//use rand::seq::SliceRandom;
//use serde::{Deserialize, Serialize};
//use url::Url;
use rand::Rng;
use async_std::task;

#[command]
pub async fn slannoy(ctx: Context, user: User) -> CommandResult {
    let mut annoy_choice = SmallRng::from_entropy();
    let mut dura_choice = SmallRng::from_entropy();

    let annoy_count: i32 = annoy_choice.gen_range(5..15);
//    let duration = Duration::from_secs(2);
    println!("Number of annoys: {}", annoy_count);

    for _n in 1..annoy_count {
        msg.channel_id.say(&ctx.http, format!("{}", user.tag())).await?;
        let duration = Duration::from_secs(dura_choice.gen_range(10..120));
        println!("Wait {} seconds", duration.as_secs().to_string());
        task::sleep(duration).await;
    }

    Ok(())
}

