use serenity::framework::standard::{macros::command, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;
use once_cell::sync::Lazy;
use markov::Chain;

//static mut CORPUS_PATH: &str = String::new("/home/kruug/bragr/src/commands/corpus");
//static mut CHAIN: Chain<String> = Chain::<String>::load(CORPUS_PATH).expect("Issues opening corpus");
static CHAIN: Lazy<Chain<String>> = Lazy::new(|| Chain::load("/home/kruug/bragr/src/commands/corpus").unwrap());

#[command]
async fn marko(ctx: &Context, msg: &Message) -> CommandResult {

    let message = CHAIN.generate();

    let mut output = String::new();
    for i in &message {
        output.push_str(i);
        output.push_str(" ");
    }

//    println!("{:?}",output);
    msg.channel_id.say(&ctx.http, output).await?;

    Ok(())
}
