use serenity::framework::standard::{macros::command, CommandResult, Args};
use serenity::model::prelude::*;
use serenity::prelude::*;
//use std::thread::sleep;
use std::time::Duration;
use rand::SeedableRng;
use rand::rngs::SmallRng;
use rand::seq::SliceRandom;
use serde::{Deserialize, Serialize};
use url::Url;
use rand::Rng;
use async_std::task;

#[derive(Serialize, Deserialize)]
struct Word {
    response: String,
}

#[derive(Serialize, Deserialize)]
struct Poem {
    line1: String,
    line2: String,
    line3: String,
}

/*error_chain! {
    foreign_links {
        Io(std::io::Error);
        HttpRequest(reqwest::Error);
    }
}*/

#[command]
async fn pong(ctx: &Context, msg: &Message) -> CommandResult {
    msg.channel_id.say(&ctx.http, "Ping!").await?;

    Ok(())
}

#[command]
async fn bingo(ctx: &Context, msg: &Message) -> CommandResult {
    msg.channel_id.say(&ctx.http, "https://i.imgur.com/fypAmty.mp4").await?;

    Ok(())
}

#[command]
async fn bongo(ctx: &Context, msg: &Message) -> CommandResult {
    msg.channel_id.say(&ctx.http, "https://i.imgur.com/Mc8hPgr.mp4").await?;

    Ok(())
}

#[command]
async fn baconade(ctx: &Context, msg: &Message) -> CommandResult {
    msg.channel_id.say(&ctx.http, "https://i.imgur.com/oxRe9An.jpg").await?;

    Ok(())
}

#[command]
async fn oingo(ctx: &Context, msg: &Message) -> CommandResult {
    msg.channel_id.say(&ctx.http, "https://www.youtube.com/watch?v=H2LQMElLoLs").await?;

    Ok(())
}

#[command]
async fn annoy(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let user = args.rest();
    let mut annoy_choice = SmallRng::from_entropy();
    let mut dura_choice = SmallRng::from_entropy();

    let annoy_count: i32 = annoy_choice.gen_range(5..15);
//    let duration = Duration::from_secs(2);
//    println!("Number of annoys: {}", annoy_count);

    for _n in 1..annoy_count {
        let duration = Duration::from_secs(dura_choice.gen_range(10..120));
        task::sleep(duration).await;
        msg.channel_id.say(&ctx.http, format!("{}", user)).await?;
//        let duration = Duration::from_secs(dura_choice.gen_range(10..120));
//        println!("Wait {} seconds", duration.as_secs().to_string());
//        task::sleep(duration).await;
    }

    Ok(())
}

#[command]
async fn pick(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let text = args.rest();
    let options: Vec<&str> = text.split(';').filter(|x| !x.is_empty()).collect();
    let mut rand_choice = SmallRng::from_entropy();

    if options.len() >= 2 {
        let choice = options.choose(&mut rand_choice).unwrap();
        let fchoice = choice.trim();

        msg.channel_id.say(&ctx.http, &format!("{}: I pick {}!", msg.author.mention(), fchoice)).await?;
    } else {
        msg.channel_id.say(&ctx.http, "`~pick something;something else[;third option[;...]]` - Randomly picks one of the given options.").await?;
    }

    Ok(())
}

#[command]
async fn haiku(ctx: &Context, msg: &Message) -> CommandResult {
    let topics = vec!["hobby", "hobbit", "story", "marvel", "comics", "potato", "food", "football", "dungeons", "dragons", "video", "games", "pool", "sand", "star", "technology", "poop"];

    let mut rand_word = SmallRng::from_entropy();
    let random_word = topics.choose(&mut rand_word).unwrap(); //[rand::thread_rng().gen_range(topics.len())];

    let mut url = "https://haiku.kremer.dev/?keyword=".to_string();
    url.push_str(&random_word);
    let parsed = Url::parse(&url)?;
    let res = reqwest::get(parsed).await?;
    let body = res.text().await?;

    let poem: Poem = serde_json::from_str(&body)?;

    let output = format!("{}\n{}\n{}\n", poem.line1, poem.line2, poem.line3);

    msg.channel_id.say(&ctx.http, output).await?;

    Ok(())
}
