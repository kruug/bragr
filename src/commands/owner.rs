use serenity::framework::standard::{macros::command, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;
use tokio::process::Command;
//use async_std::process::Stdio;
use crate::ShardManagerContainer;

#[command]
#[owners_only]
async fn quit(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;

    if let Some(manager) = data.get::<ShardManagerContainer>() {
        msg.reply(ctx, "Shutting down!").await?;
        manager.lock().await.shutdown_all().await;
    } else {
        msg.reply(ctx, "There was a problem getting the shard manager").await?;

        return Ok(());
    }

    Ok(())
}

#[command]
#[owners_only]
async fn restart(ctx: &Context, msg: &Message) -> CommandResult {
    msg.reply(ctx, "Restarting!").await?;

    let _result = Command::new("systemctl").args(&["restart", "bragr.service"]).spawn();

    Ok(())
}

#[command]
#[owners_only]
async fn uptime(ctx: &Context, msg: &Message) -> CommandResult {
    let output = Command::new("sh")
        .arg("-c")
        .arg("systemctl status bragr | grep Active | cut -d ';' -f 2")
        .output()
        .await
        .unwrap();

    let uptime = String::from_utf8_lossy(&output.stdout);

    msg.channel_id.say(&ctx.http, format!("Bot Uptime: {:?}", &uptime.trim())).await?;
    Ok(())
}
