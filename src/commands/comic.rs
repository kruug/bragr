use serenity::framework::standard::{macros::command, CommandResult, Args};
use serenity::prelude::*;
use serenity::model::channel::Message;
use serenity::model::prelude::ChannelId;
use sqlx::mysql::MySqlPoolOptions;
use serenity::futures::TryStreamExt;
use rand::random;
use once_cell::sync::OnceCell;

pub struct Comic {
    pub date: String,
    pub title: String,
    pub url: String,
    pub alt: String,
}

static COMICS: OnceCell<Vec<Comic>> = OnceCell::new();
    
pub async fn init_comics() {
    let pool = MySqlPoolOptions::new()
        .max_connections(5)
        .connect("mysql://oglaf:oglaf@192.168.1.31/oglaf")
        .await
        .expect("Successful connection to database");

    let mut rows = sqlx::query!("SELECT * FROM oglaf")
        .fetch(&pool);

    let mut comics = Vec::new();

    while let Some(row) = rows.try_next().await.unwrap() {
        let date_upload = row.date_upload.to_string();
        let comic_title = row.title.unwrap();
        let pic_url = row.pic_url.unwrap();
        let alt_text = row.alt_text.unwrap();

        let comic = Comic {
            date: date_upload,
            title: comic_title,
            url: pic_url,
            alt: alt_text,
        };

        comics.push(comic);
    }

    COMICS.set(comics).ok();
}

#[command]
async fn oglaf(ctx: &Context, _msg: &Message, _args: Args) -> CommandResult {

    let comics = COMICS.get().unwrap();
    let index = (random::<f32>() * comics.len() as f32).floor() as usize;
    let comic_choice = &comics[index];
    
    let mut full_url = "https://kruug.org/images/oglaf/".to_string();
    full_url.push_str(&comic_choice.url);

    //let _ = msg.channel_id.send_message(&ctx, |m| {
    let _ = ChannelId(665337627683061771).send_message(&ctx, |m| {
        m.embed(|e|{
            e.title(&comic_choice.title);
            e.description(&comic_choice.alt);
            e.image(full_url);
            e.thumbnail("https://kruug.org/images/oglaf/logo.png");
            e.footer(|f| {
                f.text(&comic_choice.date);
                f
            });
            e.author(|a| {
                a.name("Oglaf");
                a.icon_url("https://kruug.org/images/oglaf/OglafAuthor.png");
                a
            });
            e
        })
    }).await?;

    Ok(())
}
