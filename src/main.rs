//! Requires the 'framework' feature flag be enabled in your project's
//! `Cargo.toml`.
//!
//! This can be enabled by specifying the feature in the dependency section:
//!
//! ```toml
//! [dependencies.serenity]
//! git = "https://github.com/serenity-rs/serenity.git"
//! features = ["framework", "standard_framework"]
//! ```
mod commands;

use std::{collections::HashSet, env, sync::Arc};

use commands::{math::*, meta::*, owner::*, marko::*, fng::*, comic::*};
use serenity::{
    async_trait,
    client::bridge::gateway::ShardManager,
    framework::{standard::macros::group, StandardFramework},
    http::Http,
    model::{event::ResumedEvent, gateway::Ready, id::ChannelId},
    prelude::*,
};
use serenity::model::prelude::*;
use tracing::{error, info};
use tracing_subscriber::{EnvFilter, FmtSubscriber};
use std::path::{Path};

pub struct ShardManagerContainer;

impl TypeMapKey for ShardManagerContainer {
    type Value = Arc<Mutex<ShardManager>>;
}

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, msg: Message) {
        if msg.content.to_lowercase().contains("bees") {
            let emoji1 = ReactionType::Custom {
                animated: false,
                id: EmojiId(778074358027386892),
                name: Some("bee_happy".to_string()),
            };
            let emoji2 = ReactionType::Custom {
                animated: false,
                id: EmojiId(778072165475024897),
                name: Some("bee_mad".to_string()),
            };
            let emoji3 = ReactionType::Custom {
                animated: false,
                id: EmojiId(778072217882722324),
                name: Some("bee_sad".to_string()),
            };
            for emoji in [emoji1, emoji2, emoji3] {
                if let Err(why) = msg.react(&ctx, emoji).await {
                    eprintln!("Error reaction: {}", why);
                }
            }
        }
    }

/*    async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
        if let Interaction::ApplicationCommand(command) = interaction {
            let content = match command.data.name.as_str() {
                "slannoy" => {
                    let options = command
                        .data
                        .options
                        .get(0)
                        .expect("Expected user option")
                        .resolved
                        .as_ref()
                        .expect("Expected user object");

                    if let ApplicationCommandInteractionDataOptionValue::User(user, _member) =
                        options
                    {
                        commands::slash::slannoy(user).await;
                    } else {
                        "Please provide a valid user".to_string()
                    }
                },
                _ => "not implemented :(".to_string(),
            };
        }
    }*/

    async fn ready(&self, ctx: Context, ready: Ready) {
        commands::comic::init_comics().await;
        /*let guild_id = GuildId(
            env::var("GUILD_ID")
                .expect("Expected GUILD_ID in environment")
                .parse()
                .expect("GUILD_ID must be an integer"),
        );

        let commands = GuildId::set_application_commands(&guild_id, &ctx.http, |commands| {
            commands
                .create_application_command(|command| {
                    command.name("annoy").description("An annoying command")
                })
        }).await;*/
        info!("Connected as {}", ready.user.name);
//        ChannelId(615028576118505497).say(&ctx, "From the shadows I come!").await.expect("Error sending startup message");
        ChannelId(683009876997439541).say(&ctx, "From the shadows I come!").await.expect("Error sending startup message");
    }

    async fn resume(&self, _: Context, _: ResumedEvent) {
        info!("Resumed");
    }
}

#[group]
#[commands(multiply, ping, quit, restart, pong, bingo, bongo, oingo, annoy, pick, marko, haiku, baconade, version, time, date, oglaf, uptime)]
struct General;

#[tokio::main]
async fn main() {
    // This will load the environment variables located at `./.env`, relative to
    // the CWD. See `./.env.example` for an example on how to structure this.
    let my_path = Path::new("/home/kruug/bragr/.env");
    dotenv::from_path(my_path).expect("Failed to load .env file");
//    dotenv::dotenv().expect("Failed to load .env file");

    // Initialize the logger to use environment variables.
    //
    // In this case, a good default is setting the environment variable
    // `RUST_LOG` to debug`.
    let subscriber =
        FmtSubscriber::builder().with_env_filter(EnvFilter::from_default_env()).finish();

    tracing::subscriber::set_global_default(subscriber).expect("Failed to start the logger");

    let token = env::var("DISCORD_TOKEN").expect("Expected a token in the environment");

    let http = Http::new_with_token(&token);

    // We will fetch your bot's owners and id
    let (owners, _bot_id) = match http.get_current_application_info().await {
        Ok(info) => {
            let mut owners = HashSet::new();
            owners.insert(info.owner.id);

            (owners, info.id)
        },
        Err(why) => panic!("Could not access application info: {:?}", why),
    };

    // Create the framework
    let framework =
        StandardFramework::new().configure(|c| c.owners(owners).prefix("~")).group(&GENERAL_GROUP);

    let mut client = Client::builder(&token)
        .framework(framework)
        .event_handler(Handler)
        .await
        .expect("Err creating client");

    {
        let mut data = client.data.write().await;
        data.insert::<ShardManagerContainer>(client.shard_manager.clone());
    }

    let shard_manager = client.shard_manager.clone();

    tokio::spawn(async move {
        tokio::signal::ctrl_c().await.expect("Could not register ctrl+c handler");
        shard_manager.lock().await.shutdown_all().await;
    });

    if let Err(why) = client.start().await {
        error!("Client error: {:?}", why);
    }
}
